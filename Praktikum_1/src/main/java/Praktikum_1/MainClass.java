package Praktikum_1;
public class MainClass {

    public static void main(String[] args) {

        UKM ukm = new UKM();
        Penduduk[]agt;
        agt = new Penduduk [10];
        int n = 0;
        
        System.out.println("Unit Kegiatan Mahasiswa ");
        System.out.println("=========================================");
        
        Mahasiswa ketua = new Mahasiswa ();
        ketua.setNama("Denny Sumargo");
        ketua.setNim(9191519);
        ketua.setTanggalLahir("21 september 2001");

        Mahasiswa skr = new Mahasiswa ();
        skr.setNama("Rachel Cia");
        skr.setNim(19959545);
        skr.setTanggalLahir("14 april 1999");
        
        agt[n] = new Mahasiswa();
        ((Mahasiswa)agt[n]).setNama("Natasya Sungkono");
        ((Mahasiswa)agt[n]).setNim(941657895);
        ((Mahasiswa)agt[n]).setTanggalLahir("9 juni 2001");
        n++;
        
        agt[n] = new MasyarakatSekitar();
        ((MasyarakatSekitar)agt[n]).setNama("Deddy corbuzier");
        ((MasyarakatSekitar)agt[n]).setNomor(195);
        ((MasyarakatSekitar)agt[n]).setTanggalLahir("12 Januari 2000");
        n++;
        
        ukm.setKetua(ketua);
        ukm.setSekretaris(skr);
        ukm.setNamaUnit("Acara");
        System.out.println("" +ukm.getNamaUnit());
        System.out.println("=========================================");
        System.out.println("Ketua");
        System.out.println("Ketua\t\t : " +ukm.getKetua().getNama());
        System.out.println("Tanggal Lahir\t : "+ketua.getTanggalLah());
        System.out.println("NIM\t\t : " +ketua.getNim());
        System.out.println("-----------------------------------------");
        System.out.println("Sekertaris");
        System.out.println("Sekretaris\t : " +ukm.getSekretaris().getNama());
        System.out.println("Tanggal Lahir\t : "+skr.getTanggalLah());
        System.out.println("NIM\t\t : " +skr.getNim());
        System.out.println("=========================================");
        
    
        System.out.println("Anggota");
        for (int i = 0; i < n; i++) {
            if(agt[i]instanceof Mahasiswa){
                System.out.println("Mahasiswa");
                System.out.println("Nama\t\t: "+agt[i].getNama());
                System.out.println("NIM\t\t: "+((Mahasiswa)agt[i]).getNim());
                System.out.println("Tanggal Lahir\t: "+agt[i].getTanggalLah());
                System.out.println("Iuran\t\t: Rp"+((Mahasiswa)agt[i]).hitungIuran());  
                System.out.println("-----------------------------------------");
            }
            else {
                System.out.println("Masyarakat Sekitar");
                 System.out.println("Nama\t\t: "+agt[i].getNama());
                System.out.println("NIM\t\t: "+((MasyarakatSekitar)agt[i]).getNomor());
                System.out.println("Tanggal Lahir\t: "+agt[i].getTanggalLah());
                System.out.println("Iuran\t\t: Rp"+((MasyarakatSekitar)agt[i]).hitungIuran());  
            
            }
        }
        System.out.println("=========================================");
    }
}